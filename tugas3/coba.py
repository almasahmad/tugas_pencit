import numpy as np
import cv2
from matplotlib import pyplot as plt
import matplotlib.image as mpimg
from numpy import linalg as LA

im=cv2.imread("catur01.png")
openfile = 'A.png'
img1 = cv2.imread(openfile)

A = []

###Metode harris###
def f_harris(img):
	gray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
	gray = np.float32(gray)
	dst = cv2.cornerHarris(gray,2,3,0.04)
	
	#result is dilated for marking the corners, not important
	dst = cv2.dilate(dst,None)

	# Threshold for an optimal value, it may vary depending on the image
	img[dst>0.01*dst.max()]=[255,0,0]

	A.append(dst)

	return img

###Metode shitomasi###
def f_shiTomasi(img):
	gray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
	corners = cv2.goodFeaturesToTrack(gray,5,0.01,10)
	corners = np.int0(corners)
	for i in corners:
		x,y = i.ravel()
		cv2.circle(img,(x,y),3,255,-1)

	return img

catur1 = f_harris(im)
catur2 = f_shiTomasi(im)

plt.subplot(1,3,1),plt.imshow(im),plt.title('Input')
plt.xticks([]), plt.yticks([])
plt.subplot(1,3,2),plt.imshow(catur1),plt.title('Harris')
plt.xticks([]), plt.yticks([])
plt.subplot(1,3,3),plt.imshow(catur2),plt.title('Shi-Tomasi')
plt.xticks([]), plt.yticks([])

plt.show()

###Flip gambar###
horizontal_img = f_harris(cv2.flip( img1, 0 ))
vertical_img = f_harris(cv2.flip( img1, 1 ))
both_img = f_harris(cv2.flip( img1, -1 ))

plt.subplot(3,2,1),plt.imshow(img1)
plt.subplot(3,2,2),plt.imshow(horizontal_img)
plt.subplot(3,2,3),plt.imshow(img1)
plt.subplot(3,2,4),plt.imshow(vertical_img)
plt.subplot(3,2,5),plt.imshow(img1)
plt.subplot(3,2,6),plt.imshow(both_img)

plt.show()

###Rotate gambar###
def rotate(img1,derajat,index):
	h,w = img1.shape[:2]
	center = (w/2,h/2)

	putar = cv2.getRotationMatrix2D(center,derajat,1)

	rotatingImg = cv2.warpAffine(img1,putar,(w,h))	

	plt.subplot(2,4,index),plt.imshow(rotatingImg)

	return rotatingImg

hasil1 = f_harris(rotate(img1,-90,1))
hasil2 = f_harris(rotate(img1,90,2))
hasil3 = f_harris(rotate(img1,180,3))

plt.subplot(245),plt.imshow(hasil1)
plt.subplot(246),plt.imshow(hasil2)
plt.subplot(247),plt.imshow(hasil3)

plt.show()