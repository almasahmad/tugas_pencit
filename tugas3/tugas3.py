import numpy as np
import cv2
from matplotlib import pyplot as plt
import matplotlib.image as mpimg
from numpy import linalg as LA

N = 200
M = N

data = []
charakter = []  

stringResult = " "

im = cv2.imread('E571PY.jpg')

img1 = im[0:200,0:200]
img2 = im[0:200,200:400]
img3 = im[0:200,400:600]
img4 = im[0:200,600:800]
img5 = im[0:200,800:1000]
img6 = im[0:200,1000:1200]

###<--Menyimpan string dan merubah ukuran gamnbar-->###
def blok_query(img,string):
    img = cv2.imread(img)
    imgview = cv2.resize(img,(M,N),interpolation=cv2.INTER_AREA)

    charakter.append(string)

    return imgview

###<--Untuk mengambil haris dari gambar dan query-->###
def hitung(imgview) :
    gray = cv2.cvtColor(imgview,cv2.COLOR_BGR2GRAY)
    gray = np.float32(gray)
    dst = cv2.cornerHarris(gray,2,3,0.04)

    dst = cv2.dilate(dst,None)

    return dst

###<--Untuk mengambil data dari query-->###
def data_query(dst) :
    A=np.array(dst)
    filler,v=LA.eig(A)
    filler.sort()

    data.append(filler)

###<--Merubah ukuran gambar-->###
def blok_gambar(img,index) :
    imgview = cv2.resize(img,(M,N),interpolation=cv2.INTER_AREA)

    plt.subplot(5,6,index),plt.imshow(imgview)

    return imgview

###<--Hasil yang di keluarkan-->###
def hasil(img,dst,index) :
    img[dst>0.01*M]=[0,0,255]
    plt.subplot(5,6,6+index), plt.imshow(img)
    A=np.array(dst)
    filler,v=LA.eig(A)
    filler.sort()
    sementara = []
    C=[]
    detectedWord = " "
    for x in range (0,len(charakter)):
        C = abs(data[x]-filler)
        # print("c = ",C)
        sementara.append([sum(C),charakter[x]])
        
    sementara.sort()

    detectedWord = sementara[0][1]

    plt.subplot(5,6,6+index), plt.text(0.5, 0.5, detectedWord, \
    horizontalalignment='center',verticalalignment='center')

    return detectedWord

###<--Untuk memutar gambar-->###
def rotate(img1,derajat):
	h,w = img1.shape[:2]
	center = (w/2,h/2)

	putar = cv2.getRotationMatrix2D(center,derajat,1)

	rotatingImg = cv2.warpAffine(img1,putar,(w,h))	

	# plt.subplot(2,4,index),plt.imshow(rotatingImg)

	return rotatingImg

###<--Memanggil fungsi rotate jika ingin di hidupkan-->###
# img1 = rotate(img1,180)
# img2 = rotate(img2,180)
# img3 = rotate(img3,180)
# img4 = rotate(img4,180)
# img5 = rotate(img5,180)
# img6 = rotate(img6,180)

###<--Untuk menyimpan data query-->###
data_query(hitung(blok_query("abjad/A.jpg","A")))
data_query(hitung(blok_query("abjad/B.jpg","B")))
data_query(hitung(blok_query("abjad/C.jpg","C")))
data_query(hitung(blok_query("abjad/D.jpg","D")))
data_query(hitung(blok_query("abjad/E.jpg","E")))
data_query(hitung(blok_query("abjad/F.jpg","F")))
data_query(hitung(blok_query("abjad/G.jpg","G")))
data_query(hitung(blok_query("abjad/H.jpg","H")))
data_query(hitung(blok_query("abjad/I.jpg","I")))
data_query(hitung(blok_query("abjad/J.jpg","J")))
data_query(hitung(blok_query("abjad/K.jpg","K")))
data_query(hitung(blok_query("abjad/L.jpg","L")))
data_query(hitung(blok_query("abjad/M.jpg","M")))
data_query(hitung(blok_query("abjad/N.jpg","N")))
data_query(hitung(blok_query("abjad/O.jpg","O")))
data_query(hitung(blok_query("abjad/P.jpg","P")))
data_query(hitung(blok_query("abjad/Q.jpg","Q")))
data_query(hitung(blok_query("abjad/R.jpg","R")))
data_query(hitung(blok_query("abjad/S.jpg","S")))
data_query(hitung(blok_query("abjad/T.jpg","T")))
data_query(hitung(blok_query("abjad/U.jpg","U")))
data_query(hitung(blok_query("abjad/V.jpg","V")))
data_query(hitung(blok_query("abjad/W.jpg","W")))
data_query(hitung(blok_query("abjad/X.jpg","X")))
data_query(hitung(blok_query("abjad/Y.jpg","Y")))
data_query(hitung(blok_query("abjad/Z.jpg","Z")))

data_query(hitung(blok_query("angka/0.jpg","0")))
data_query(hitung(blok_query("angka/1.jpg","1")))
data_query(hitung(blok_query("angka/2.jpg","2")))
data_query(hitung(blok_query("angka/3.jpg","3")))
data_query(hitung(blok_query("angka/4.jpg","4")))
data_query(hitung(blok_query("angka/5.jpg","5")))
data_query(hitung(blok_query("angka/6.jpg","6")))
data_query(hitung(blok_query("angka/7.jpg","7")))
data_query(hitung(blok_query("angka/8.jpg","8")))
data_query(hitung(blok_query("angka/9.jpg","9")))

###<--Untuk menyimpan hasil-->###
stringResult = stringResult + hasil(blok_gambar(img1,1),hitung(blok_gambar(img1,1)),1)
stringResult = stringResult + hasil(blok_gambar(img2,2),hitung(blok_gambar(img2,2)),2)
stringResult = stringResult + hasil(blok_gambar(img3,3),hitung(blok_gambar(img3,3)),3)
stringResult = stringResult + hasil(blok_gambar(img4,4),hitung(blok_gambar(img4,4)),4)
stringResult = stringResult + hasil(blok_gambar(img5,5),hitung(blok_gambar(img5,5)),5)
stringResult = stringResult + hasil(blok_gambar(img6,6),hitung(blok_gambar(img6,6)),6)

print(stringResult)

plt.subplot(6,1,6), plt.text(0.5, 0.5, stringResult, \
horizontalalignment='center',verticalalignment='center')

plt.show()