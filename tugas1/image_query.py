import numpy as np
from PIL import Image
import cv2 as cv
from matplotlib import pyplot as plt
import matplotlib.image as mpimg

imSize=100

##<<--IMPORT GAMBAR-->>##
img1 = mpimg.imread('putih.jpg')
img2 = mpimg.imread('mangga.jpg')
img3 = mpimg.imread('naga.jpg')
img4 = mpimg.imread('bunga matahari.jpg')
imgsearch = mpimg.imread('pic(search).jpg')
cluster = cv.imread('pelangi.jpg')

##<<--CLUSTER-->>##
clusterquery = mpimg.imread('pelangi.jpg')
K = 6
Zclusterquery = clusterquery.reshape((-1,3))
Zclusterquery = np.float32(Zclusterquery)
criteriaclusterquery = (cv.TERM_CRITERIA_EPS + cv.TERM_CRITERIA_MAX_ITER, 10, 1.0)
ret,label,clusterquery=cv.kmeans(Zclusterquery,K,None,criteriaclusterquery,10,cv.KMEANS_RANDOM_CENTERS)
clusterquery = np.int16(clusterquery)
hitamputih = np.array([[0,0,0],[255,255,255]])
clusterquery = np.concatenate((clusterquery,hitamputih))

K = K + 2
print(clusterquery)

##<<--GETTING PIXEL-->>##
def getting(gambar) :
    img = cv.resize(gambar,(imSize,imSize))
    pixel=[]
    for x in range(0,imSize):
        for y in range(0,imSize) :
            a=img[x,y]
            pixel.append(a)
    pixel = np.int16(pixel)

    return pixel
    
pixel1 = getting(img1)
pixel2 = getting(img2)
pixel3 = getting(img3)
pixel4 = getting(img4)
pixelsearch = getting(imgsearch)

##<<--COMPARING WITH CLUSTER-->>##
def compire(pixel,index) :
    graph=[]
    for x in range(0,imSize*imSize):
        selectedK = 0
        rgbCount = 0
        for z in range(0,3):
            pixelCount = abs(clusterquery[0][z]-pixel[x][z])
            rgbCount=rgbCount+pixelCount
        rgbHuge = rgbCount
        for y in range (0,K) :
            rgbCount = 0
            for z in range(0,3):
                pixelCount = abs(clusterquery[y][z]-pixel[x][z])
                rgbCount=rgbCount+pixelCount
            if rgbHuge > rgbCount :
                rgbHuge = rgbCount
                selectedK = y
        graph.append(selectedK)
    histogram = len(graph)
    # print(graph)
    plt.subplot(4,4,index), plt.hist(graph,K,facecolor = 'green')
    res, bins, patches = plt.hist(graph, K, facecolor = 'green')
    # print(res)

    return res

res1 = compire(pixel1,5)
res2 = compire(pixel2,6)
res3 = compire(pixel3,7)
res4 = compire(pixel4,8)
ressearch = compire(pixelsearch,11)

##<<--COUNTING NEAREST OBJECT-->>##

try1=abs(res1-ressearch)
try2=abs(res2-ressearch)
try3=abs(res3-ressearch)
try4=abs(res4-ressearch)

arr1=[[sum(try1),img1],[sum(try2),img2],[sum(try3),img3],[sum(try4),img4]]
arr1.sort()

##<<--SHOW PICTURE-->>##
def cetak_gambar(gambar,index, nama) :
    plt.subplot(4,4,index),plt.imshow(gambar)
    plt.title(nama), plt.xticks([]), plt.yticks([])

cetak_gambar(img1,1,'1st Pic')
cetak_gambar(img2,2,'2st Pic')
cetak_gambar(img3,3, '3st Pic')
cetak_gambar(img4,4, '4st Pic')
cetak_gambar(imgsearch,10, 'query')

plt.subplot(4,4,9),plt.imshow([clusterquery]) 
plt.title('Query'), plt.xticks([]), plt.yticks([])

def hasil(gambar,index,nama) :
    plt.subplot(4,4,index),plt.imshow(arr1[gambar][1])
    plt.title(nama), plt.xticks([]), plt.yticks([])

hasil(0,13,'1st Res')
hasil(1,14,'2st Res')
hasil(2,15,'3st Res')
hasil(3,16, '4st Res')

plt.show()