import numpy as np
import cv2
from matplotlib import pyplot as plt
import matplotlib.image as mpimg
from PIL import Image, ImageDraw
# M --> Mendatar
# N --> Menurun
N = 400
M = N
luasBlok = 20
a=0
b=0
c=0
d=0
e=0
f=0
g=0

data = []
charakter = []  
baru = []

stringResult = " "

###Untuk memfilter isi box 100 dibagi dg angka###
PersenFilter = 4

im = cv2.imread('E571PY.jpg')

img1 = im[0:200,0:200]
img2 = im[0:200,200:400]
img3 = im[0:200,400:600]
img4 = im[0:200,600:800]
img5 = im[0:200,800:1000]
img6 = im[0:200,1000:1200]

result = np.zeros((N,M,3), np.uint8)
result.fill(255)

###Blok Papan Catur <AWAL>###
blockx = M//2
blocky = N//2
for y1 in range (0,blocky):
    if y1 % 2 == 0:
        for x1 in range(0,blockx):
            if x1 % 2 == 0 :
                #kotak di genap
                result[luasBlok*y1:luasBlok*y1+luasBlok , luasBlok*x1:luasBlok*x1+luasBlok] = (0,0,0)

for y2 in range (0,blocky):
    if y2 % 2 != 0:
        for x2 in range(0,blockx):
            if x2 % 2 != 0 :
                #kotak di genap
                result[luasBlok*y2:luasBlok*y2+luasBlok , luasBlok*x2:luasBlok*x2+luasBlok] = (0,0,0)

###Blok terpilih query###
def blok_query(namaGambar,string) :
    img = cv2.imread(namaGambar)
    imgview = cv2.resize(img,(M,N),interpolation=cv2.INTER_AREA)

    result2 = np.zeros((N,M,3), np.uint8)
    result2.fill(255)
    
    for i in range (0,N//luasBlok):
        for j in range (0,N//luasBlok):
            a = i * luasBlok 
            b = j * luasBlok
            blockcount = 0
            for a in range (a,a + luasBlok):
                for b in range (b,b+luasBlok):
                    R,G,B = imgview[a,b]
                    if R != 255 and G != 255 and B != 255:
                        blockcount = blockcount + 1
                b = b - luasBlok+1
            a = i * luasBlok 
            b = j * luasBlok
            if blockcount > (luasBlok * luasBlok / PersenFilter):
                result2[ a : a + luasBlok , b : b + luasBlok] = (0,0,0)
    
    charakter.append(string)

    return result2

###Blok terpilih gambar###
def blok_gambar(img,index) :
    imgview = cv2.resize(img,(M,N),interpolation=cv2.INTER_AREA)

    plt.subplot(6,6,6*1+index),plt.imshow(imgview)

    result2 = np.zeros((N,M,3), np.uint8)
    result2.fill(255)

    for i in range (0,N//luasBlok):
        for j in range (0,N//luasBlok):
            a = i * luasBlok 
            b = j * luasBlok
            blockcount = 0
            for a in range (a,a + luasBlok):
                for b in range (b,b+luasBlok):
                    R,G,B = imgview[a,b]
                    if R != 0 and G != 0 and B != 0:
                        blockcount = blockcount + 1
                b = b - luasBlok+1
            a = i * luasBlok 
            b = j * luasBlok
            if blockcount > (luasBlok * luasBlok / PersenFilter):
                result2[ a : a + luasBlok , b : b + luasBlok] = (0,0,0)
    
    return result2

###Menghitung nilai gambar###
def hitung(result2) :
    counter = 0
    baruku=0
    graph=[]
    deteksi=[] 
    for i in range (0,N//luasBlok):
        for j in range (0,N//luasBlok):
            c = i * luasBlok 
            d = j * luasBlok 
            R,G,B = result2[c,d]
            if R != 255 and G != 255 and B != 255: 
                graph.append(counter)
                baruku = baruku + 1
        counter = counter + 1
        baru.append(baruku)
        baruku = 0

    for i in range (0,N//luasBlok):
        for j in range (0,N//luasBlok):
            e = i * luasBlok 
            f = j * luasBlok 
            R,G,B = result2[f,e]
            if R != 255 and G != 255 and B != 255: 
                graph.append(counter)
                baruku = baruku + 1
        counter = counter + 1
        baru.append(baruku)
        baruku = 0

    counter = counter - 1

    # print ("n2 = ",baru)

    return graph

###Data query###
def data_query(graph) :
    histogram = len(graph)
    
    n2, bins1, patches = plt.hist(graph, 10) 

    data.append(n2)

###Hasil yang di keluarkan###
def hasil(graph,index) :
    plt.subplot(6,6,6*2+index), plt.hist(graph,39,[0,39])

    histogram = len(graph)

    n2, bins1, patches = plt.hist(graph, 10) 

    sementara = []
    C=[]
    detectedWord = " " 
    for x in range (0,len(charakter)):
        C = abs(data[x]-n2)
        sementara.append([sum(C),charakter[x]])
        
    sementara.sort()

    detectedWord = sementara[0][1]

    plt.subplot(6,6,6*4+index), plt.text(0.5, 0.5, detectedWord, \
    horizontalalignment='center',verticalalignment='center')

    return detectedWord

###menampilkan garis pic 3###
def tempel(result2,index) :
    for i in range (0,luasBlok):##garis mendatar
        cv2.line(result2,(0,luasBlok*i),(M,luasBlok*i),(0,0,0),2)
    for i in range (0,luasBlok):##garis vertical	
        cv2.line(result2,(luasBlok*i,0),(luasBlok*i,N),(0,0,0),2)

    plt.subplot(6,6,6*3+index),plt.imshow(result2)
    
data_query(hitung(blok_query("abjad/A.jpg","A")))
data_query(hitung(blok_query("abjad/B.jpg","B")))
data_query(hitung(blok_query("abjad/C.jpg","C")))
data_query(hitung(blok_query("abjad/D.jpg","D")))
data_query(hitung(blok_query("abjad/E.jpg","E")))
data_query(hitung(blok_query("abjad/F.jpg","F")))
data_query(hitung(blok_query("abjad/G.jpg","G")))
data_query(hitung(blok_query("abjad/H.jpg","H")))
data_query(hitung(blok_query("abjad/I.jpg","I")))
data_query(hitung(blok_query("abjad/J.jpg","J")))
data_query(hitung(blok_query("abjad/K.jpg","K")))
data_query(hitung(blok_query("abjad/L.jpg","L")))
data_query(hitung(blok_query("abjad/M.jpg","M")))
data_query(hitung(blok_query("abjad/N.jpg","N")))
data_query(hitung(blok_query("abjad/O.jpg","O")))
data_query(hitung(blok_query("abjad/P.jpg","P")))
data_query(hitung(blok_query("abjad/Q.jpg","Q")))
data_query(hitung(blok_query("abjad/R.jpg","R")))
data_query(hitung(blok_query("abjad/S.jpg","S")))
data_query(hitung(blok_query("abjad/T.jpg","T")))
data_query(hitung(blok_query("abjad/U.jpg","U")))
data_query(hitung(blok_query("abjad/V.jpg","V")))
data_query(hitung(blok_query("abjad/W.jpg","W")))
data_query(hitung(blok_query("abjad/X.jpg","X")))
data_query(hitung(blok_query("abjad/Y.jpg","Y")))
data_query(hitung(blok_query("abjad/Z.jpg","Z")))

data_query(hitung(blok_query("angka/0.jpg","0")))
data_query(hitung(blok_query("angka/1.jpg","1")))
data_query(hitung(blok_query("angka/2.jpg","2")))
data_query(hitung(blok_query("angka/3.jpg","3")))
data_query(hitung(blok_query("angka/4.jpg","4")))
data_query(hitung(blok_query("angka/5.jpg","5")))
data_query(hitung(blok_query("angka/6.jpg","6")))
data_query(hitung(blok_query("angka/7.jpg","7")))
data_query(hitung(blok_query("angka/8.jpg","8")))
data_query(hitung(blok_query("angka/9.jpg","9")))

stringResult = stringResult + hasil(hitung(blok_gambar(img1,1)),1)
stringResult = stringResult + hasil(hitung(blok_gambar(img2,2)),2)
stringResult = stringResult + hasil(hitung(blok_gambar(img3,3)),3)
stringResult = stringResult + hasil(hitung(blok_gambar(img4,4)),4)
stringResult = stringResult + hasil(hitung(blok_gambar(img5,5)),5)
stringResult = stringResult + hasil(hitung(blok_gambar(img6,6)),6)

tempel(blok_gambar(img1,1),1)
tempel(blok_gambar(img2,2),2)
tempel(blok_gambar(img3,3),3)
tempel(blok_gambar(img4,4),4)
tempel(blok_gambar(img5,5),5)
tempel(blok_gambar(img6,6),6)

print(stringResult)

plt.subplot(6,1,6), plt.text(0.5, 0.5, stringResult, \
    horizontalalignment='center',verticalalignment='center')

plt.show()