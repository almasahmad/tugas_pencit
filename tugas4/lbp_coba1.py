import numpy as np
import cv2
from matplotlib import pyplot as plt
import matplotlib.image as mpimg

baru = []
threshold = 5

image_file = 'jokowi.jpg'
img_bgr = mpimg.imread(image_file)
height, width, channel = img_bgr.shape
img_gray = cv2.cvtColor(img_bgr, cv2.COLOR_BGR2GRAY)

def get_pixel(img, center, x, y):
    new_value = 1
    try:
        if img[x][y] >= center:
            new_value = 0
    except:
        pass
    return new_value

def seleksi (img, x, y) :
    center = img[x][y] - threshold
    val_ar = []
    val_ar.append(get_pixel(img, center, x-1, y+1))     # top_right
    val_ar.append(get_pixel(img, center, x, y+1))       # right
    val_ar.append(get_pixel(img, center, x+1, y+1))     # bottom_right
    val_ar.append(get_pixel(img, center, x+1, y))       # bottom
    val_ar.append(get_pixel(img, center, x+1, y-1))     # bottom_left
    val_ar.append(get_pixel(img, center, x, y-1))       # left
    val_ar.append(get_pixel(img, center, x-1, y-1))     # top_left
    val_ar.append(get_pixel(img, center, x-1, y))       # top
    
    power_val = [1, 2, 4, 8, 16, 32, 64, 128]
    val = 0
    for i in range(len(val_ar)):
        val += val_ar[i] * power_val[i]
    
    minimum = 255
    if minimum > val :
        minimum = val

    return minimum

def lbp_calculated_pixel(minimum):
    a=0

    if minimum == 255 :
        a = 0
    elif minimum == 127 :
        a = 1
    elif minimum == 63 :
        a = 2
    elif minimum == 31 :
        a = 3
    elif minimum == 15 :
        a = 4
    elif minimum == 7 :
        a = 5
    elif minimum == 3 :
        a = 6
    elif minimum == 1 :
        a = 7
    elif minimum == 0 :
        a = 8
    
    # print("a=",a)
    
    return a

img_lbp = np.zeros((height, width,3), np.uint8)
for i in range(0, height):
        for j in range(0, width):
            img_lbp[i, j] = seleksi(img_gray, i, j)
            baru.append(lbp_calculated_pixel(seleksi(img_gray, i, j)))

# print(baru)

plt.subplot(131), plt.imshow(img_bgr)
plt.subplot(132), plt.imshow(img_lbp)
plt.subplot(133), plt.hist(baru,10,facecolor = 'green')
plt.show()